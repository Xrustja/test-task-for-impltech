import Characters from "./components/Characters";
import './App.scss';


function App() {
    return (
        <div className="App">
            <h1 className="title">Star Wars characters discussions</h1>
            <Characters/>
        </div>
    );
}

export default App;
