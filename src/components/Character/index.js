import React, {useState} from "react";
import CommentsList from "../CommentsList";
import "./styles.scss";

const Character = ({name, birth_year}) => {

    const [showDiscussion, setShowDiscussion] = useState(false);

    const openComments = () => {
        setShowDiscussion(!showDiscussion);
    };

    return (
        <div key={name} className="character">
            <div className="character__container">
                <div className="character__description">
                    <h3> {name}</h3>
                    <p className="character__birth"><i>Year of birth:</i> {birth_year}</p>
                </div>
                <button className="btn" onClick={openComments}>{showDiscussion ? "Hide " : "Open "}discussion</button>
            </div>
            <CommentsList character={name} show={showDiscussion}/>
        </div>
    )
}

export default Character;
