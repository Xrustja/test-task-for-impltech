import React, {useEffect, useState} from "react";
import APIRequests from "../../services/APIRequests";
import getCharacters from "../../services/getCharacters";
import Character from "../Character";
import "./styles.scss";

const {get} = APIRequests;

const Characters = () => {
    const [characters, setCharacters] = useState([]);
    const [page, setPage] = useState('');
    const [loading, setLoading] = useState(false);
    const [hideBtn, setHideBtn] = useState(false);

    const loadMore = () => {
        setLoading(true);
        get(page)
            .then(({results: nextCharacters, next}) => {
                setCharacters([...characters, ...nextCharacters]);
                setPage(next);
                next === null ? setHideBtn(true) : setHideBtn(false);
                setLoading(false);
            })
    };

    useEffect(() => {
        let cleanupFunction = false;
        getCharacters()
            .then(({results: characters, next}) => {
                if (!cleanupFunction) {
                    setCharacters(characters || []);
                    setPage(next || null)
                }
            })
        return () => {
            cleanupFunction = true
        }
    }, []);

    return (
        <div className="characters">
            {characters.length === 0
                ?
                <div className="characters__loading">
                    <i className="fa fa-spinner fa-spin"></i>
                </div>
                :
                <>
                    {characters.map(character => {
                        return (
                            <Character key={character.name} name={character.name} birth_year={character.birth_year}/>
                        )
                    })}
                    <button className={`btn btn-loadMore ${hideBtn ? 'hidden' : ''}`} onClick={loadMore}>
                        {loading ? <i className="fa fa-spinner fa-spin"></i> : ''}Load more
                    </button>
                </>
            }
        </div>
    )
}

export default Characters;