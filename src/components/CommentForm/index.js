import {useState} from "react";
import "./styles.scss";

const CommentForm = ({addNew}) => {
    const [name, setName] = useState('');
    const [comment, setComment] = useState('');
    const [nameValid, setNameValid] = useState(false);
    const [commentValid, setCommentValid] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        let time = new Date().toLocaleString() + '';
        if (name !== "" && comment !== "") {
            let newComment = {name, comment, time}
            addNew(newComment);
            setName('');
            setComment('');
        } else {
            if (name === '') {
                setNameValid(true)
            }
            if (comment === '') {
                setCommentValid(true)
            }
        }
    };

    return (
        <div className="comment-form__container">
            <h3>Leave your comment:</h3>
            <form id="comment-form" className="comment-form" onSubmit={e => {
                handleSubmit(e)
            }}>
                <input className="comment-form__author" placeholder="Enter your name" size={50} name="name" type="text"
                       value={name}
                       onChange={e => {
                           setName(e.target.value);
                           setNameValid(false)
                       }}
                />
                {(nameValid) && <div className="error">Enter your name</div>}

                <textarea className="comment-form__text" placeholder="Leave your comment" name="comment"
                          value={comment}
                          onChange={e => {
                              setComment(e.target.value);
                              setCommentValid(false);
                          }}/>
                {(commentValid) && <div className="error">Enter your comment</div>}

                <button className="btn comment-form__btn" type="submit">Add comment</button>
            </form>
        </div>
    )
}

export default CommentForm;