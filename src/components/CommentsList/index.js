import {useEffect, useState} from "react";
import * as LocalStorageServices from "../../services/LocalStorageServices";
import CommentForm from "../CommentForm";
import "./styles.scss";

const CommentsList = ({character, show}) => {
    const [comments, setComments] = useState([]);

    const addNewComments = (newComment) => {
        const newCommentsList = [...comments, newComment];
        LocalStorageServices.setLSData(character, newCommentsList);
        setComments(newCommentsList);
    };

    useEffect(() => {
        if (LocalStorageServices.getLSData(character) !== null) {
            const commentsInLS = LocalStorageServices.getLSData(character);
            setComments(commentsInLS);
        }
    }, [character]);

    return (
        <div className={`${show ? '' : 'hidden'}`}>
            {comments.map((comment, id) => {
                return (
                    <div className="comment__container" key={id}>
                        <div className="comment__info">
                            <div className="comment__info__author">
                                <i className="fas fa-user-astronaut"></i>
                                <span>{comment.name}</span>
                            </div>
                            <p className="comment__info__time">{comment.time}</p>
                        </div>
                        <p className="comment__text">{comment.comment}</p>
                    </div>
                )
            })}
            <CommentForm addNew={addNewComments}/>
        </div>
    )
}

export default CommentsList;